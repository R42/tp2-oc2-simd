#include "filtros.h"
#include <stdio.h>

// intel simd
#if defined(__SSE2__)
#include <emmintrin.h>
#endif

#define min(a,b) (((a) < (b)) ? (a) : (b))
#define max(a,b) (((a) > (b)) ? (a) : (b))

FILTRO SEPIA = { 
	.393, .760, .189,
	.349, .686, .168,
	.272, .534, .131
};

/******************************************************************************/

void filtro (BMPDATA *bmpData, FILTRO filtro) {

	for (int i=0; i<cantPixels(bmpData); i++) {

		unsigned char r = bmpData->red[i];
		unsigned char g = bmpData->green[i];
		unsigned char b = bmpData->blue[i];
		bmpData->red[i]   = min (r * filtro.RR + g * filtro.RG + b * filtro.RB, 255);
		bmpData->green[i] = min (r * filtro.GR + g * filtro.GG + b * filtro.GB, 255);
		bmpData->blue[i]  = min (r * filtro.BR + g * filtro.BG + b * filtro.BB, 255);
	}
}

/******************************************************************************/

void blancoYNegro (BMPDATA *bmpData) {
	
	for (int i=0; i<cantPixels(bmpData); i++) {
	
		unsigned char y = bmpData->red[i] * 0.11448f + bmpData->green[i] * 0.58661f + bmpData->blue[i] * 0.29891f;
		bmpData->red[i]   = y;
		bmpData->green[i] = y;
		bmpData->blue[i]  = y;
	}	
}

/******************************************************************************/


void aclarar(BMPDATA *bmpData,long inc){
	//si se va a valores (0,-255) se debe oscurecer
	// si va a valores de (0,255) se tiene que aclarar
	inc=max((-255),min(255,inc));

	for (int i=0; i<cantPixels(bmpData); i++) {
		unsigned char r = bmpData->red[i];
		unsigned char g = bmpData->green[i];
		unsigned char b = bmpData->blue[i];

		bmpData->red[i]	= max(0,min(r+inc,255));
		bmpData->green[i] = max(0,min(g+inc,255));
		bmpData->blue[i] = max(0,min(b+inc,255)); 
	}
}

/******************************************************************************/

void medianFilter(BMPDATA *bmpData){
	//necesito el alto y ancho y se que lo tiene el Header
	int N= bmpData->infoHeader.biHeight; //alto
	int M= bmpData->infoHeader.biWidth; //ancho

	//declaro el tamaño de la ventana
	int sizeWindow=9;

	//como no me fijo los bordes,lo trunco a entero
	int mitadWindow = (int)(sizeWindow/2);

	for(int h=1;h<N-1;h++){
		for(int w=1;w<M-1;w++){
			int k=0;
			//agarro un color de la ventana
			unsigned char r[sizeWindow];
			unsigned char g[sizeWindow];
			unsigned char b[sizeWindow];
			
			for(int i=h-1;i<h+2;i++){
				for(int j=w-1;j<w+2;j++){
					r[k]=bmpData->red[(i*M)+j];
					g[k]=bmpData->green[(i*M)+j];
					b[k]=bmpData->blue[(i*M)+j];
					k++;
				}
			}
			//ahora tengo que ordenar cada color para elegir el medio y reemplazarlos
			//Rojo
			for (int i=0;i<sizeWindow;i++){
				int min=i;
				for (int j=i+1;j<sizeWindow;j++){
					if(r[j]<r[min])
						min=j;
				}
				const unsigned char aux=r[i];
				r[i]=r[min];
				r[min]=aux;
			}
			bmpData->red[(h-1)*M+(w-1)]=r[4];

			//Verde
			for (int i=0;i<sizeWindow+1;i++){
				int min=i;
				for (int j=i+1;j<sizeWindow;j++){
					if(g[j]<g[min])
						min=j;
				}
				const unsigned char aux=g[i];
				g[i]=g[min];
				g[min]=aux;
			}
			bmpData->green[(h-1)*M+(w-1)]=g[4];

			//Azul
			for (int i=0;i<sizeWindow+1;i++){
				int min=i;
				for (int j=i+1;j<sizeWindow;j++){
					if(b[j]<b[min])
						min=j;

				}
				const unsigned char aux=b[i];
				b[i]=b[min];
				b[min]=aux;
			}
			bmpData->blue[(h-1)*M+(w-1)]=b[4];
		}
	}	
}

/******************************************************************************/

void multiplyBlend(BMPDATA *bmpData, BMPDATA *bmpData2){
	int cantPixelImagen1=cantPixels(bmpData);
	int cantPixelImagen2=cantPixels(bmpData2);
	if(cantPixelImagen1 != cantPixelImagen2){
		printf("Las imagenes deben tener el mismo tamaño\n");
	}else{
		// multiplico los pixeles y los guardo en la imagen1 
		for (int i=0;i<cantPixels(bmpData);i++){
			bmpData->red[i]=max(0, min(bmpData->red[i] + bmpData2->red[i], 255));
			bmpData->green[i]=max(0, min(bmpData->green[i] + bmpData2->green[i], 255));
			bmpData->blue[i]=max(0, min(bmpData->blue[i] + bmpData2->blue[i], 255));
		}
	}
}

/******************************************************************************/

void negativo (BMPDATA *bmpData) {
	
	for (int i=0; i<cantPixels(bmpData); i++) {
		bmpData->red[i]   = -bmpData->red[i];
		bmpData->green[i] = -bmpData->green[i];
		bmpData->blue[i]  = -bmpData->blue[i];
	}	
}

/******************************************************************************/

void grisesNegativo(BMPDATA *bmpData){
	for (int i=0; i<cantPixels(bmpData); i++) {
	
		unsigned char y = bmpData->red[i] * -0.11448f + bmpData->green[i] * -0.58661f + bmpData->blue[i] * -0.29891f;
		bmpData->red[i]   = y;
		bmpData->green[i] = y;
		bmpData->blue[i]  = y;

	}
}

/******************************************************************************/

unsigned char mediana (unsigned char *histo, int imediana) {

	int k, aux=0;
	for (k=0; k<255 && aux<=imediana; k++)
		aux += histo[k];

	return k;
}

/******************************************************************************/

int cantPixels(BMPDATA *bmpData) {

	return bmpData->infoHeader.biWidth * bmpData->infoHeader.biHeight;
}
	
