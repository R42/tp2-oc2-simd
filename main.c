#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "lectorBmp.h"
#include "filtros.h"


/*
 * Funciones en asembler
 */
//extern void blendAsm (char *, char *, char *, int, int, int, int);


// DOC_HELP



extern int asmPrint
(char * msg, int lenght);

int asm_Print(char * msg){
  return asmPrint(msg,strlen(msg));
}
/******************************************************************************/


int main (int argc, char* argv[]) {
	asm_Print("Organización del Computador 2.\nTrabajo Práctico Nro. 2\nPrograma para procesamiento de imágenes BMP.\nPor Amengual Lautaro y Juarez Veronica\n");
	int resolucion = 1080;
	BMPDATA bmpData;
	BMPDATA bmpData2;
	BMPDATA bmpData3;

	int i;

	/*printf("Ejecutando con parametros: ");
	for (i=1 ; i<argc; i++){
		printf("%s ", argv[i]);
	}*/

	// carga el archivo bmp
	if (loadBmpFile ("lena.bmp", &bmpData) != 0) {
		printf ("Error al leer el archivo %s\n\n", "lena.bmp");
		return 1;
	}

	clock_t start, end;
	
	// comienza a medir el tiempo
	start = clock();

	//para cada filtro debo cargar la imagen, llamar al filtro y guardar la imagen
	//para el filtro blanco y negro
	blancoYNegro (&bmpData);

	if(saveBmpFile("lenaGris.bmp",&bmpData)!=0){
		asm_Print("Error al guardar el archivo");
	}

	//para el filtro SEPIA
	if (loadBmpFile ("lena.bmp", &bmpData) != 0) {
		printf ("Error al leer el archivo %s\n\n", "lena.bmp");
		return 1;
	}
	filtro(&bmpData, SEPIA);

	if(saveBmpFile("lenaSepia.bmp",&bmpData)!=0){
		asm_Print("Error al guardar el archivo");
	}
	
	//para el filtro aclarar
	if (loadBmpFile ("lena.bmp", &bmpData) != 0) {
		printf ("Error al leer el archivo %s\n\n", "lena.bmp");
		return 1;
	}
	aclarar(&bmpData,50);

	if(saveBmpFile("lenaClara.bmp",&bmpData)!=0){
		asm_Print("Error al guardar el archivo");
	}

	//para el filtro medianFilter
	if (loadBmpFile ("sample_corrupted.bmp", &bmpData) != 0) {
		printf ("Error al leer el archivo %s\n\n", "sample_corrupted.bmp");
		return 1;
	}
	medianFilter(&bmpData);

	if(saveBmpFile("sample_nuevo.bmp",&bmpData)!=0){
		asm_Print("Error al guardar el archivo");
	}
	if (loadBmpFile ("lena_corrupted.bmp", &bmpData) != 0) {
		printf ("Error al leer el archivo %s\n\n", "lena_corrupted.bmp");
		return 1;
	}
	medianFilter(&bmpData);

	if(saveBmpFile("lena_corrupted_nuevo.bmp",&bmpData)!=0){
		asm_Print("Error al guardar el archivo");
	}

	//para el filtro multiplyBlend
	//cargo las 2 imagenes para luego pasarlo por parametros

	if (loadBmpFile ("imagen1.bmp", &bmpData2) != 0) {
		printf ("Error al leer el archivo %s\n\n", "imagen1.bmp");
		return 1;
	}
	if (loadBmpFile ("imagen2.bmp", &bmpData3) != 0) {
		printf ("Error al leer el archivo %s\n\n", "imagen2.bmp");
		return 1;
	}
	multiplyBlend(&bmpData2,&bmpData3);

	if(saveBmpFile("imagen12.bmp",&bmpData2)!=0){
		asm_Print("Error al guardar el archivo");
	}

	//para el filtro negativo (extra)
	if (loadBmpFile ("lena.bmp", &bmpData) != 0) {
		printf ("Error al leer el archivo %s\n\n", "lena.bmp");
		return 1;
	}
	negativo(&bmpData);

	if(saveBmpFile("lenaNegativo.bmp",&bmpData)!=0){
		asm_Print("Error al guardar el archivo");
	}

	//para el filtro grises y negativo (extra)
	if (loadBmpFile ("lena.bmp", &bmpData) != 0) {
		printf ("Error al leer el archivo %s\n\n", "lena.bmp");
		return 1;
	}
	grisesNegativo(&bmpData);

	if(saveBmpFile("lenaGrisNegativo.bmp",&bmpData)!=0){
		asm_Print("Error al guardar el archivo");
	}


	end = clock();
	FILE *out = fopen("results.csv", "a");  
	int tiempo = end-start;
	fprintf(out, "%d %s %d", resolucion, " tiempo: ", tiempo );
  	fclose(out); 

	// imprime tiempo
	
	printf("\nTiempo de proceso: %ld ticks.\n\n", end-start);

	if (saveBmpFile ("lena.bmp", &bmpData) != 0)
		asm_Print("Error al grabar el archivo!");
	
	// libera memoria
	limpiarBmpData(&bmpData);
	return 0;
}
